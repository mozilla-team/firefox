# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# "Search" is a verb, as in "Search through tabs".
all-tabs-menu-search-tabs =
    .label = Przeszukaj karty
all-tabs-menu-new-user-context =
    .label = Nowa karta z kontekstem
all-tabs-menu-hidden-tabs =
    .label = Ukryte karty
all-tabs-menu-manage-user-context =
    .label = Zarządzaj kontekstami
    .accesskey = Z
all-tabs-menu-close-duplicate-tabs =
    .label = Zamknij podwójne karty
all-tabs-menu-close-all-duplicate-tabs =
    .label = Zamknij wszystkie podwójne karty
all-tabs-menu-synced-tabs =
    .label = Karty z innych urządzeń
all-tabs-menu-current-window-header = Bieżące okno
# "Show all" is for showing all open groups as well as saved groups. Initially,
# we only show up to five of these groups.
all-tabs-menu-tab-groups-show-all =
    .label = Wyświetl wszystkie
